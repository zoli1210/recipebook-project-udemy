import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthResponseData, AuthService } from '../services/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit{

  authForm!: FormGroup;

  isLoginMode = true;
  isLoading = false;
  error: string = null;
  regMessage = false;

  constructor(private authService: AuthService, private router: Router){}

  ngOnInit(): void {

    this.authForm = new FormGroup({
      'email' : new FormControl('', [Validators.required, Validators.email]),
      'password' : new FormControl('', [Validators.required, Validators.minLength(6)])
    })
  }
  onSwitchMode(){
    this.isLoginMode = !this.isLoginMode;
  }

  onSubmit(){
    if(!this.authForm.valid){
      return;
    }

    const email = this.authForm.value.email;
    const password = this.authForm.value.password;

    let authObservable: Observable<AuthResponseData>

    this.isLoading = true;

    if (this.isLoginMode){
      authObservable = this.authService.login(email, password)
    }else{
      authObservable = this.authService.signUp(email,password)
    }

    authObservable.subscribe(
      data => {
        console.log(data);
        this.isLoading = false;
        this.router.navigate(['/recipes']);
      },
      errorMessage => {
        this.error = errorMessage;
        this.isLoading = false;
      }
    );
    this.regMessage = true;
    this.authForm.reset();
  }
}
