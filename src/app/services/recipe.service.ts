import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Ingredient } from '../common/ingredient';
import { Recipe } from '../common/recipe';
import { ShoppingListService } from './shopping-list.service';

@Injectable({
  providedIn: 'root'
})
@Injectable()
export class RecipeService {

  recipesChanged = new Subject<Recipe[]>();

  // recipes: Recipe[] = [
  //   new Recipe (
  //    'Gnocchi',
  //    'Simply Taste',
  //    'https://assets.bonappetit.com/photos/60a4022a248102a01bcfa0b6/1:1/w_1920,c_limit/0621-Sheet-Pan-Gnocchi.jpg',
  //    [
  //      new Ingredient('Meat',1),
  //      new Ingredient('French Fries',20)
  //    ])
  // ];
  private recipes: Recipe[] = [];

  constructor(private slService: ShoppingListService){}

  setRecipes(recipes: Recipe[]){
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }

  getRecipes(){
    return this.recipes.slice();
  }

  getRecipe(index: number){
    return this.recipes[index];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe){
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updatedRecipe(index: number, newRecipe: Recipe){
    this.recipes[index] = newRecipe;
    this.recipesChanged.next(this.recipes.slice());
  }
  deletedRecipe(index:number){
    this.recipes.splice(index, 1);
    this.recipesChanged.next(this.recipes.slice());
  }
}
